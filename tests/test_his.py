import os
import unittest
import his


class TestIDF(unittest.TestCase):
    def setUp(self):
        self.his = 'test.his'
        self.his_out = 'test-out.his'

    def tearDown(self):
        try:
            os.remove(self.his_out)
        except FileNotFoundError:
            pass

    def test_his(self):
        pn = his.read(self.his)
        his.write(self.his_out, pn)


if __name__ == '__main__':
    unittest.main()
